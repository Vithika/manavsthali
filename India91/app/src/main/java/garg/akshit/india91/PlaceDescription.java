package garg.akshit.india91;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ms.square.android.expandabletextview.ExpandableTextView;

public class PlaceDescription extends AppCompatActivity implements OnMapReadyCallback {

    PlaceInfo currentPlace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_description);

        currentPlace = (PlaceInfo) getIntent().getSerializableExtra("CLICKED_PLACE");

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(currentPlace.getName());


        ImageView i = (ImageView)findViewById(R.id.place);

        Glide.with(this).load(currentPlace.getTopimage()).into(i);


        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        ExpandableTextView expTv1 = (ExpandableTextView) findViewById(R.id.expand_text_view);


        expTv1.setText(currentPlace.getDescription());


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        SliderLayout sliderShow = (SliderLayout) findViewById(R.id.slider);
        sliderShow.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));


        DefaultSliderView textSliderView = new DefaultSliderView(this);
        textSliderView.image(currentPlace.getTopimage()).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
        sliderShow.addSlider(textSliderView);
        sliderShow.setDuration(1500);

        DefaultSliderView textSliderView2 = new DefaultSliderView(this);
        textSliderView2.image(currentPlace.getSecondimage()).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
        sliderShow.addSlider(textSliderView2);
        sliderShow.setDuration(1500);

        DefaultSliderView textSliderView3 = new DefaultSliderView(this);
        textSliderView3.image(currentPlace.getThirdimage()).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
        sliderShow.addSlider(textSliderView3);
        sliderShow.setDuration(1500);

        DefaultSliderView textSliderView4 = new DefaultSliderView(this);
        textSliderView4.image(currentPlace.getFourthimage()).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
        sliderShow.addSlider(textSliderView4);
        sliderShow.setDuration(1500);


        TextView addr = (TextView)findViewById(R.id.address);
        addr.setText(currentPlace.getAddress());


        LinearLayout call_layer = (LinearLayout) findViewById (R.id.call_layout);
        TextView phNo = (TextView)findViewById(R.id.phone_No);
        if(!currentPlace.getPhoneno().equals("0"))
            phNo.setText(currentPlace.getPhoneno());
        else
            call_layer.setVisibility(View.GONE);


        call_layer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" +  currentPlace.getPhoneno()));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        String name = currentPlace.getName();

        // Add a marker at location and move the camera.
        LatLng coordinates = new LatLng(currentPlace.getLattitude(), currentPlace.getLongitude());
        googleMap.addMarker(new MarkerOptions().position(coordinates).title(name));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(coordinates));
    }
}
